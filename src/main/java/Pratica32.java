
public class Pratica32 {
    public static double densidade(double x, double media,double desvio){
        double d = (1/(Math.sqrt(2 * Math.PI) * desvio)) * Math.exp((-1.0/2.0) * Math.pow(((x - media)/desvio),2));
        return d;
    }
    public static void main(String[] args) {
        System.out.println(densidade(-1.0,67.0,3.0));
    }
}
